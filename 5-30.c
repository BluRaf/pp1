#include <stdio.h>

int is_prime(unsigned int a)
{
  if (a < 2) {
    return -1;
  }
  else {
    int divisors = 0;
    for (int i = 2; i < a; i++) {
      if (a%i == 0) {
        divisors++;
      }
    }
    if (divisors) {
      return 0; /* not a prime number */
    }
    else {
      return 1; /* prime number */
    }
  }
}

int main()
{
  int array[1001];
  int buf;
  int counter = 0;

  for (counter = 0; counter < 1001; counter++) {
    scanf("%d", &buf);
    if (buf == 0)
      break;
    else
      array[counter] = buf;
  }

  for (int i = 0; i < counter; i++) {
    if (is_prime(i) == 1) {
      printf("%d ", array[i]);
    }
  }

  printf("\n");

  return 0;
}
