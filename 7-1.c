#include <stdio.h>

int test1(int a)
{
  return a;
}

int main()
{
  int input;
  printf("Podaj argument: ");
  scanf("%d", &input);
  int output = test1(input);
  printf("Wynik funkcji: %d\n", output);
  return 0;
}

