#include <stdio.h>

int main()
{
  int values[] = {2, 1, 3, 7, -1}; /* zakładam, że algorytm będzie przetwarzał tylko liczby naturalne, więc użyłem liczby ujemnej -1 do oznaczenia końca ciągu */
  int counter = 0;
  for (int i=0;;i++) {
    if (values[i] != -1)
      counter++;
    else
      break;
  }
  printf("%d\n", counter);

  /* Q: Czy w języku C istnieje funkcja podająca długość tablicy?
   * A: Pojedyncza funkcja? Nie. Ale jeśli nie jest to tablica dynamiczna, można obliczyć długość tablicy jako podzielenie ilości bajtów zajmowaną przez tablicę przez ilość bajtów zajmowaną przez typ pojedynczej wartości. Obie te wartości można wyciągnąć funkcją sizeof().
   * przykład:
   */

  printf("%d\n", sizeof(values)/sizeof(int)-1); /* -1, bo tablica w 5 linii ma de facto 5 elementów, wliczając -1 */

  return 0;
}
