#include <stdio.h>
#include <math.h>

int is_prime(unsigned int a)
{
  if (a < 2) {
    return -1;
  }
  else {
    int divisors = 0;
    for (int i = 2; i < a; i++) {
      if (a%i == 0) {
	divisors++;
      }
    }
    if (divisors) {
      return 0; /* not a prime number */
    }
    else {
      return 1; /* prime number */
    }
  }
}
	
int main()
{
  int values[10];
  for (int i = 0; i < 10; i++) {
    scanf("%d", &values[i]);
  }
  for (int j = 0; j < 10; j++) {
    printf("%d", values[j]);
    if (is_prime(values[j])) {
      printf(", liczba pierwsza");
    }
    printf("\n");
  }
  return 0;
}
