#include <stdio.h>
#include <math.h>

float average(int tab[], int size)
{
  float sum = 0.0;
  for (int i = 0; i < size; i++) {
    sum += tab[i];
  }
  return sum/size;
}

float std_deviation(int tab[], int size)
{
  float aver = average(tab, size);
  float sum = 0.0;
  float deviation;
  for (int i=0; i < size; i++) {
    sum += pow(tab[i]-aver,2);
    deviation = sqrt(sum/size);
  }
  return deviation;
}

int main()
{
  int tab[1000];
  int buf;
  int i = 0;
  
  printf("Podaj liczby: ");
  for (i = 0; i < 1000; i++) {
    scanf("%d", &buf);
    if (buf) {
      tab[i] = buf;
    }
    else {
      break;
    }
  }
  
  printf("%.2f\n", average(tab, i));
  printf("%.2f\n", std_deviation(tab, i));
  
  return 0;
}
