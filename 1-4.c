#include <stdio.h>

int main()
{
  int integer = 1337;
  float single_precision = 3.1415927;
  double double_precision = 3.141592653589793;

  printf("integer=%i\n", integer);
  printf("single_precision=%f\n", single_precision);
  printf("double_precision=%lf\n", double_precision);

  return 0;
}
