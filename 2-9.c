#include <stdio.h>

int main()
{
  int a, b;
  
  printf("Liczba? ");
  scanf("%d", &a);
  printf("Liczba? ");
  scanf("%d", &b);

  printf("%d\n", a+b);
  printf("%d\n", a-b);
  printf("%d\n", a*b);
  if (b == 0) {
    printf("Dividing by zero is not supported in this universe.\n");
  }
  else {
    printf("%d\n", a/b);    
  }

  return 0;
}
