#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define RAND_LOW 0
#define RAND_HIGH 100

int main()
{
  srand((unsigned int)time(NULL));
  num = (rand() % (RAND_HIGH - RAND_LOW + 1) + RAND_LOW);
  int i;
  int buf;
  for (i=0; i<10; i++) {
  	printf("? ");
    scanf("%d", &buf);
    if (buf > num)
      printf("za duzo\n");
    if (buf < num)
      printf("za malo\n");
    if (buf == num)
      printf("trafione!\n");
      return 0;
  }
  printf("przegrana\n");
  
  return 0;
}

