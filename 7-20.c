#include <stdio.h>
#include <math.h>

float distance(int x1, int y1, int x2, int y2)
{
  return sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) );
}

float perimeter(float a, float b, float c)
{
  return a+b+c;
}

float area(float a, float b, float c)
{
  return sqrt(     (perimeter(a,b,c)/2) 
               * ( (perimeter(a,b,c)/2)-a ) 
               * ( (perimeter(a,b,c)/2)-b ) 
               * ( (perimeter(a,b,c)/2)-c )
              );
}

int main()
{
int ax, ay;
int bx, by;
int cx, cy;

printf("x pierwszego punktu: ");
scanf("%d", &ax);
printf("y pierwszego punktu: ");
scanf("%d", &ay);
printf("x drugiego punktu: ");
scanf("%d", &bx);
printf("y drugiego punktu: ");
scanf("%d", &by);
printf("x trzeciego punktu: ");
scanf("%d", &cx);
printf("y trzeciego punktu: ");
scanf("%d", &cy);

int ab = distance(ax, ay, bx, by);
int ac = distance(ax, ay, cx, cy);
int bc = distance(bx, by, cx, cy);

printf("%d\n", area(ab, ac, bc));

return 0;

}

