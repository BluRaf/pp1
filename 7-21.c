#include <stdio.h>
#include <math.h>

float rad(float deg)
{
  return deg*(M_PI/180);
}

float deg(float rad)
{
  return rad*(180/M_PI);
}

int main()
{
  int mode;
  printf("Wybierz rodzaj konwersji (1=stopnie na radiany, 2=radiany na stopnie): ");
  scanf("%d", &mode);

  float angle;
  printf("Podaj miare kata: ");
  if (!scanf("%f", &angle)) {
    printf("Error\n");
    return 1;
  }
  
  switch (mode)
  {
    case 1:
      printf("Wynik: %.2f\n", rad(angle));
      return 0;
    case 2:
      printf("Wynik: %.2f\n", deg(angle));      
      return 0;
    default:
      printf("Error\n");
      return 1;
  }
}
