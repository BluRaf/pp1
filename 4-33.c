#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

#define RAND_LOW -1000
#define RAND_HIGH 1000

int main()
{
  int n = 0;
  srand((unsigned int)time(NULL));
  for (int i = 0; i < 100; i++) {
    int num = (rand() % (RAND_HIGH - RAND_LOW + 1) + RAND_LOW);
    if (abs(num) <= 100) {
      n++;
    }
  }
  return 0;
}

