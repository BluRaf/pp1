#include <stdio.h>

#define MAX 1000

int main()
{
  int tab[MAX];
  printf("podaj liczby: ");
  int i = 0;
  for (i=0; i<MAX; i++) {
    int buf;
    scanf("%d", &buf);
    if (buf) tab[i] = buf;
    else break;
  }

  int o = 0;                          // pozycja w tablicy
  for (o = 0; o < i; o++) {           // dla każdej pozycji w tablicy
    int duplicate_found = 0;          // flaga "czy wystąpił duplikat"
    for (int j = o; j<i; j++) {       // dla każdej pozycji w tablicy
      if ((tab[j] == tab[o]) && j!=o) duplicate_found=1; 
      // jeśli duplikat (ta sama liczba na różnych poz.), ustaw flagę
    }   
    if (duplicate_found) {               // jeśli był duplikat
      int mentioned_previously = 0;      // sprawdź, czy nie był wcześniej
      for (int k = 0; k < o; k++) {      // pokazywany (od pocz. do ob. poz.)
        if (tab[k] == tab[o]) {          // jeśli była wcześniej ta sama liczba,
          mentioned_previously = 1;      // to była już wyświetlona
        }
      }
      if (!mentioned_previously) printf("%d ", tab[o]);
    }   
  }
  
  printf("\n");
  return 0;
}
