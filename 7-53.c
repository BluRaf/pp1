#include <stdio.h>

#define MAX 150

int concat_begin(int first[], int size, int second[], int size2, int dest[], int size3)
{
  if (size+size2 > size3) {
    return -1;
  }
  else {
    int counter = 0;
    for (int i = 0; i < size2; i++) {
      dest[i] = second[i];
      counter++;
    }
    for (int j = 0; j < size; j++) {
      dest[size2+j] = first[j];
      counter++;
    }
    return counter;
  }
}

int concat_end(int first[], int size, int second[], int size2, int dest[], int size3)
{
  if (size+size2 > size3) {
    return -1;
  }
  else {
    int counter = 0;
    for (int i = 0; i < size; i++) {
      dest[i] = first[i];
      counter++;
    }
    for (int j = 0; j < size2; j++) {
      dest[size+j] = second[j];
      counter++;
    }
    return counter;
  }
}

int concat_zip(int first[], int size, int second[], int size2, int dest[], int size3)
{
  if (size+size2 > size3) {
    return -1;
  }
  else {
    int i = 0;
    int j = 0;
    int counter = 0;
    for (counter = 0; counter < size+size2;) {
      if (i < size) {
        dest[counter] = first[i];
        i++;
        counter++;
      }
      if (j < size2) {
        dest[counter] = second[j];
        j++;
        counter++;
      }
    }
    return counter;
  }
}

int read_vector(int vec[], int size, int stop_value)
{
  int i = 0;
  int buf;
  for (i = 0; i < size; i++) {
    scanf("%d", &buf);
    vec[i] = buf;
    if (buf == stop_value) break;
  }
  return i;
}

void display_vector(int vec[], int size)
{
  for (int i = 0; i<size; i++) {
    printf("%d ", vec[i]);
  }
  printf("\n");
}

int main()
{
  int a[MAX];
  int b[MAX];
  int c[MAX];

  printf("podaj pierwszy wektor: ");
  int a_size = read_vector(a, MAX, 0);
  printf("podaj drugi wektor: ");
  int b_size = read_vector(b, MAX, 0);

  display_vector(a, a_size);
  display_vector(b, b_size);

  int c_size;
  /* W przykładzie najpierw wykonywało się concat_end, potem concat_begin.
   * Tutaj wykonuje się najpierw concat_begin, potem concat_end,
   * jak wymienione w treści zadania.
   */
  c_size = concat_begin(a, a_size, b, b_size, c, MAX);
  if (c_size < 0) printf("Error\n");
  else display_vector(c, c_size);
  
  c_size = concat_end(a, a_size, b, b_size, c, MAX);
  if (c_size < 0) printf("Error\n");
  else display_vector(c, c_size);
  
  c_size = concat_zip(a, a_size, b, b_size, c, MAX);
  if (c_size < 0) printf("Error\n");
  else display_vector(c, c_size);

  return 0;
}
