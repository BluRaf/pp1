#include <stdio.h>

int pobierz_w_zakresie(char var, int min, int max)
{
  int input;
  while(1) {
    printf("Podaj %c: ", var);
    scanf("%d", &input);
    if (input < min || input > max) {
      printf("Wartosc poza zakresem. Sprobuj ponownie.\n");
    }
    else {
      return input;
    }
  }
}

int wielomian (int a, int b, int c, int x)
{
  return ((a*x*x) + (b*x) + c);
}

int main()
{
  int a = pobierz_w_zakresie('a', -20, 20);
  int b = pobierz_w_zakresie('b', -100, 300);
  int c = pobierz_w_zakresie('c', -1000, 4000);
  int x = pobierz_w_zakresie('x', -10, 10);
  
  printf("f(x) = a*(x^2) + b*x + c\n");
  printf("f(%d) = %d*(%d^2) + %d*%d + %d\n", x, a, x, b, x, c);
  printf("f(%d) = %d\n", x, wielomian(a, b, c, x));
  
  return 0;
}

