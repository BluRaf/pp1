#include <stdio.h>
#include <math.h>

int main()
{
  int a, b, c;
  int delta;

  scanf("%d", &a);
  scanf("%d", &b);
  scanf("%d", &c);

  delta = (b*b) - (4*a*c);

  if (delta < 0) {
    printf("brak\n");
  }
  if (delta == 0) {
    printf("jeden\n");
    printf("x = %f", (float)(0-b)/(2*a));
  }
  if (delta > 0) {
    printf("dwa\n");
    printf("x_1 = %f\n", (float)(0 - b - sqrt(delta)) / (2 * a));
    printf("x_2 = %f\n", (float)(0 - b + sqrt(delta)) / (2 * a));
  }
  if (a == 0 && b == 0 && c == 0) {
    printf("nieskonczonosc\n");
  }

  return 0;
}
