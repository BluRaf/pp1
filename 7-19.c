#include <stdio.h>
#include <math.h>

int goldbach(int x)
{
  int a;
  int n;
  int j = 0;
  int primes[1000] = { 0 };
  for (a = 2; a <= x; a++) {
    int not_prime = 0;
    for (n = 2; n*n <= a; n++) {
      if (a%n == 0) {
        not_prime = 1;
      }
    }
    if (!not_prime) {
      primes[j] = a;
      j++;
    }
  }
  int number = 0;
  int i = 0;
  for (i = 0; i < j; i++) {
    for (n = 0; n < j; n++) {
      if (primes[i] + primes[n] == x) {
        printf("(%d, %d) ", primes[i], primes[n]);
        number++;
      }
    }
  }
  if (number == 0) {
    return 0;
  }
  else {
    return 1;
  }
}

int main()
{
  int x1, x2;
  int min, max;

  printf("x1 = ");
  scanf("%d", &x1);
  printf("x2 = ");
  scanf("%d", &x2);

  if (x1 < x2) {
    min = x1;
    max = x2;
  }
  else {
    min = x2;
    max = x1;
  }

  for (int i = min; i <= max; i++,i++) {
    printf("%d: ", i);
    if (!goldbach(i)) printf("Hipoteza Goldbacha nie sprawdza się dla liczby %d\n", i);
    printf("\n");
  }

  printf("\n");
  return 0;
}
