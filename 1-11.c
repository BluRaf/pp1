#include <stdio.h>
#include <math.h>

int main()
{
  int vec[3];
  int vec_symb[3] = {'a', 'b', 'c'};

  for (int i = 0; i < 3; i++) {
    printf("%c = ", vec_symb[i]);
    scanf("%d", &vec[i]);
  }

  printf("%lf\n", sqrt((vec[0]*vec[0])+(vec[1]*vec[1])+(vec[2]*vec[2])));
  
  return 0;
}
