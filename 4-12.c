#include <stdio.h>

int main()
{
  int n;
  printf("n = ");
  scanf("%d", &n);
  
  int n_f = 1;
  for (int i = 1; i <= n; i++) {
  	n_f *= i;
  }
  
  printf("n! = %d\n", n_f);
}

