#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define RAND_LOW 10
#define RAND_HIGH 20

int main()
{
  int sum;
  srand((unsigned int)time(NULL));
  for (int i=0; i<100; i++) {
    sum += (rand() % (RAND_HIGH - RAND_LOW + 1) + RAND_LOW);
  }
  
  printf("%d", sum);
  
  return 0;
}

