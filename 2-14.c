#include <stdio.h>

int main()
{
  int i = 0;
  float buff;
  float max, min, average;
  
  printf("Podaj liczby zmiennoprzecinkowe.\n"
	 "Aby zakończyć wprowadzanie, podaj liczbę -1.\n");
  for (i = 0; buff != -1; i++) {
    scanf("%f", &buff);
    if (i == 0) {
      max = buff;
      min = buff;
      average = buff;
    }
    else {
      if (buff != -1) {
        if (buff < min)
          min = buff;
        if (buff > max)
          max = buff;
        average += buff;
      }
    }
  }

  if (i > 0)
    average = average/i;

  if (i > 0) {
    printf("%.2f\n", min);
    printf("%.2f\n", max);
    printf("%.2f\n", average);
  }

  return 0;
}
