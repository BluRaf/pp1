#include <stdio.h>

int main()
{
  float iloczyn = 1.0;
  float buf;
  int scanf_ret;

  for (;;) {
    printf("* ");
    scanf_ret = scanf("%f", &buf);
    if (scanf_ret < 1) {
      printf("Blad danych wejsciowych\n");
      break;
    }
    else {
      iloczyn *= buf;
      printf("Wynik: %f\n", iloczyn);
    }
  }
}
