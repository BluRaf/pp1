#include <stdio.h>

int main()
{
  int values[10];
  for (int i = 0; i < 10; i++) {
    scanf("%d", &values[i]);
  }
  for (int j = 0; j < 10; j++) {
    printf("%d, ", values[j]);
  }
  printf("\n");
  return 0;
}
