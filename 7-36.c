#include <stdio.h>
#include <stdlib.h>
#include <math.h>

float fclamp(float v, float lo, float hi)
{
  if (v < lo) return lo;
  else if (v > hi) return hi;
  else return v;
}

float average(float tab[], int size)
{
  float sum = 0.0;
  for (int i = 0; i < size; i++) {
    sum += tab[i];
  }
  return sum/size;
}

float std_deviation(float tab[], int size)
{
  float aver = average(tab, size);
  float sum = 0.0;
  float deviation;
  for (int i=0; i < size; i++) {
    sum += pow(tab[i]-aver,2);
    deviation = sqrt(sum/size);
  }
  return deviation;
}

int main()
{
  float tab[1000];
  int buf;
  int i = 0;
  
  printf("Podaj liczby: ");
  for (i = 0; i < 1000; i++) {
    scanf("%d", &buf);
    if (buf) {
      tab[i] = buf*1.0;
    }
    else {
      break;
    }
  }
  
  float aver = average(tab, i);
  float stddev = std_deviation(tab, i);
  
  for (int j = 0; j < i; j++) {
    float delta = tab[j] - aver;
    if (abs(delta) > stddev) {
      if (delta < 0){
        tab[j] = aver - stddev;
      }
      else if (delta > 0) {
        tab[j] = aver + stddev;
      }
    }
  }
printf("%f, %f\n", aver, stddev);
  for (int k = 0; k < i; k++) {
    printf("%.2f ", tab[k]);
  }
  printf("\n");
  return 0;
}
