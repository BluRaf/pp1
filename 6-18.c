#include <stdio.h>
#include <stdlib.h>

int main() 
{
  int a[5][5];
  int b[5][5];
  int c[5][5];
  int i, j;
  
  for (i = 0; i < 5; i++) {
    for (j = 0; j < 5; j++) {
      scanf("%d", &a[i][j]);
    }
  }
  printf("\n");
  
  for (i = 0; i < 5; i++) {
    for (j = 0; j < 5; j++) {
      scanf("%d", &b[i][j]);
    }
  }
  
  for (i = 0; i < 5; i++) {
    for (j = 0; j < 5; j++) {
      if (a[i][j] > b[i][j]) {
        c[i][j] = a[i][j];
      }
      else {
        c[i][j] = b[i][j];
      }
    }
  }
  printf("\n");
  
  for (i = 0; i < 5; i++) {
    for (j = 0; j < 5; j++) {
      printf("%4d", c[i][j]);
    }
    printf("\n");
  }
  return 0;
}
