#include <stdio.h>
#include <string.h>

#define is_letter(a) ((96 < a && a < 123) || (65 < a && a < 133))
#define is_digit(a) (47 < input[i] && input[i] < 58)

int main()
{
  char input[1000];
  scanf("%s", &input);
  int input_len = strlen(input);

  char buf = 0;

  for (int i = input_len; i >= 0; i--) {
    if (is_letter(input[i])) {
      // litera
      if (is_digit(input[i-1]) || input[i+1] != 0) {
        printf("%c", input[i]);
      }
    }
    else if (is_digit(input[i])) {
      // cyfra
      for (int j = 0; j < input[i]-48; j++) {
        printf("%c", input[i+1]);
      }
    }
  }
  printf("\n");
  return 0;
}
