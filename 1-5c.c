#include <stdio.h>

int main()
{
  float fPI = 3.14159265359;
  double dPI = 3.14159265359;

  printf("%.3f\n", fPI);
  printf("%.3lf\n", dPI);

  return 0;
}
