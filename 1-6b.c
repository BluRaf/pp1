#include <stdio.h>

int main()
{
  float a = 5;
  float b = 4;

  printf("%f %f", a, b);

  printf("%.2f", a*b);
  printf("%.2f", a/b);
  printf("%.2f", a+b);
  printf("%.2f", a-b);

  return 0;
}
