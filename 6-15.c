#include <stdio.h>
#include <stdlib.h>

int main() 
{
  int tab[5][5];
  int i,j;
  int c=0,n=0,k=4,l=4,suma,suma2;
  
  for (i = 0; i < 5; i++) {
    for (j = 0; j < 5; j++) {
      scanf("%d", &tab[i][j]);
    }
  }
  
  for (i = 0; i < 5; i++) {
    for (j = n; j <= c; j++) {
      suma += tab[i][j];
    }
    n++;
    c++;
  }
  
  for (i = 0; i < 5; i++) {
    for(j=k;j<=l;j++) {
      suma2 += tab[i][j];
    }
    k--;
    l--;
  }
  
  printf("%d %d\n", suma, suma2);
  
  return 0;
}
