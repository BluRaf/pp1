#include <stdio.h>
#include "vector_utils.h"

#define MAX 150

void insertion_sort_asc(int tab[], int size)
{
  int i, x, c, n;
  for (x = 1; x < size; x++) {
    n = x;
    for (i = x - 1; i >= 0; i--) {
      if (tab[i] > tab[x]) {
        c = tab[i];
        tab[i] = tab[x];
        tab[x] = c;
        x = i;
      }
    }
    x = n;
    display_vector(tab, size);
  }
}

void insertion_sort_desc(int tab[], int size)
{
  int i, x, c, n;
  for (x = 1; x < size; x++) {
    n = x;
    for (i = x - 1; i >= 0; i--) {
      if (tab[i] < tab[x]) {
        c = tab[i];
        tab[i] = tab[x];
        tab[x] = c;
        x = i;
      }
    }
    x = n;
    display_vector(tab, size);
  }
}

void insertion_sort(int tab[], int size, int dir)
{
  switch (dir) {
  case 1:
    insertion_sort_asc(tab, size);
    break;
  case 2:
    insertion_sort_desc(tab, size);
    break;
  default:
    printf("Error\n");
    break;
  }
}

int main()
{
  int vec[MAX];
  printf("Podaj wektor: ");
  int vec_size = read_vector(vec, MAX, 0);
 
  int dir;
  printf("Podaj kierunek sortowania: ");
  scanf("%d", &dir);

  insertion_sort(vec, vec_size, dir);

  return 0;
}
