#include <stdio.h>

int equal3(int a, int b, int c)
{
  if (a == b) {
    if (a == c) {
      return 1;
    }
    else {
      return 0;
    }
  }
  else {
    return 0;
  }
}

int main()
{
  int input[3];
  
  printf("Podaj trzy liczby: ");
  for (int i=0; i<3; i++) {
    scanf("%d", &input[i]);
  }
  
  if (equal3(input[0], input[1], input[2])) {
    printf("TAK\n");
  }
  else {
    printf("NIE\n");
  }
  
  return 0;
}

