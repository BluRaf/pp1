#include <stdio.h>
#include <math.h>


int silnia(int n) {
  if (n == 1 || n == 0) return 1;
  else return n*silnia(n-1);
}

float my_sin(float x, int n) {
  float sinus = 0.0;
  for (int k = 0; k <= n; k++) {
    sinus += (pow(-1, k)/silnia((2*k) + 1)) * pow(x, (2*k) + 1);
  }
  return sinus;
}

float my_cos(float x, int n) {
  float cosinus = 0.0;
  for (int k = 0; k <= n; k++) {
    cosinus += pow(-1, k)/silnia(2*k) * pow(x, 2*k);
  }
  return cosinus;
}

int main()
{
  float input_angle;
  int input_words;

  printf("Podaj wartosc kata, dla ktorego chcesz obliczyc wartosc funkcji sin i cos: ");
  scanf("%f", &input_angle);
  printf("Podaj liczbe wyrazow szeregu Taylora: ");
  scanf("%d", &input_words);

  printf("%f %f\n", 
         my_sin(input_angle, input_words), 
         my_cos(input_angle, input_words)
         );

}

