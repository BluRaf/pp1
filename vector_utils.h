extern int concat_begin(int first[], int size, int second[], int size2, int dest[], int size3);
extern int concat_end(int first[], int size, int second[], int size2, int dest[], int size3);
extern int concat_zip(int first[], int size, int second[], int size2, int dest[], int size3);
extern int read_vector(int vec[], int size, int stop_value);
extern void display_vector(int vec[], int size);
extern int find_min(int tab[], int size, int start_index);
extern int find_max(int tab[], int size, int start_index);
