#include <stdio.h>
#include "vector_utils.h"

#define MAX 150

void bubble_sort(int tab[], int size)
{
  int buf;
  int n = size;
  for (n = size; n > 1; n--) {
    for (int i = 0; i < n-1; i++) {
      if (tab[i] > tab[i+1]) {
        buf = tab[i];
        tab[i] = tab[i+1];
        tab[i+1] = buf;
      }
    }
    display_vector(tab, size);
  }
}

int main()
{
  int vec[MAX];
  printf("Podaj wektor: ");
  int vec_size = read_vector(vec, MAX, 0);
  
  bubble_sort(vec, vec_size);
  
  display_vector(vec, vec_size);
  
  return 0;
}
