#include <stdio.h>
#include <stdlib.h>

int main() 
{
  int tab[5][5];
  int i, j;
  int sumak = 0, sumaw = 0;
  
  for (i = 0; i < 5; i++) {
    for (j = 0; j < 5; j++) {
      scanf("%d", &tab[i][j]);
    }
  }
  
  for (i = 0; i < 5; i++) {
    for (j = 0; j <= 0; j++) {
      sumaw += tab[i][j];
    }
    for (j = 4; j <= 4; j++) {
      sumaw += tab[i][j];
    }
  }

  for (i = 1; i < 4; i++) {
    for (j = 0; j <= 0; j++) {
      sumak += tab[j][i];
    }
    for (j = 4; j <= 4; j++) {
      sumak += tab[j][i];
    }
  }
  
  printf("%d\n", sumaw + sumak);
  return 0;
}
