#include <stdio.h>

int main()
{
  int number;
  const char *words[11] = {
    "zero",
    "jeden",
    "dwa",
    "trzy",
    "cztery",
    "piec",
    "szesc",
    "siedem",
    "osiem",
    "dziewiec",
    "dziesiec"
  };

  scanf("%d", &number);

  if (number < 0 || number > 10)
    printf("blad\n");
  else
    printf("%s\n", words[number]);

  return 0;
}
