#include <stdio.h>
#include "vector_utils.h"

void selection_sort_desc(int tab[], int size)
{
  int buf;
  int index_max;
  for (int i = 0; i < size; i++) {
    index_max = find_max(tab, size, i);
    if (index_max > i) {
      buf = tab[i];
      tab[i] = tab[index_max];
      tab[index_max] = buf;
    }
  }
  display_vector(tab, size);
}

void selection_sort_asc(int tab[], int size)
{
  int buf; 
  int index_min;
  for (int i = 0; i < size; i++) {
    index_min = find_min(tab, size, i);
    if (index_min > i) {
      buf = tab[i];
      tab[i] = tab[index_min];
      tab[index_min] = buf;
    }
  }
  display_vector(tab, size);	
}

void selection_sort(int tab[], int size, int dir)
{
  switch (dir) {
  case 1:
    selection_sort_asc(tab, size);
    break;
  case 2:
    selection_sort_desc(tab, size);
    break;
  default:
    printf("Error\n");
    break;
  }
}

int main()
{
  int vec[MAX];
  printf("Podaj wektor: ");
  int vec_size = read_vector(vec, MAX, 0);

  int dir;
  printf("Podaj kierunek sortowania: ");
  scanf("%d", &dir);

  selection_sort(vec, vec_size, dir);

  return 0;
}
