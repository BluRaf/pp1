#include <stdio.h>
#include <string.h>

#define MAX 1001

int starts_with(char tab[], char tab2[])
{
  int counter = 0;
  for (int i = 0; i < strlen(tab2); i++) {
    if (tab[i] == tab2[i]) counter++;
    else break;
  }
  if (counter == strlen(tab2)) return 1;
  else return 0;
}

int ends_with(char tab[], char tab2[])
{
  int counter = 0;
  int i = 0;
  for (i = 0; i < strlen(tab2); i++) {
    if (tab[strlen(tab)-strlen(tab2)+i] != tab2[i]) break;
  }
  if (i == strlen(tab2)) return 1;
  else return 0;
}

int main()
{
  char input[MAX];
  printf("Podaj tekst: \n");
  scanf("%s", &input);

  char input2[MAX];
  printf("Podaj drugi tekst do porownania: \n");
  scanf("%s", &input2);

  printf("start %d\n", starts_with(input, input2));
  printf("end %d\n", ends_with(input, input2));

  return 0;
}
