#include <stdio.h>

int main()
{
  float buf, max=0, min=0;
  float average = 0;
  int i = 0;
  
  do {
  	printf("Liczba nr %d: ", i+1);
  	scanf("%f", &buf);
  	
  	if (buf == -1) {
  	  break;
    }
  	
	if (i==0) {
  	  max = buf;
  	  min = buf;
    }
    
    if (buf > max) {
      max = buf;
	}
	
    if (buf < min) {
      min = buf;
	}
    average += buf;
    i++;
  }
  while (buf != -1);
  
  if (i>0) {
    average /= i;
    printf("%.2f\n", min);
    printf("%.2f\n", max);
    printf("%.2f\n", average);
  }
  return 0;
}

