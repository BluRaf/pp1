#include <stdio.h>

int main()
{
  int number;

  scanf("%d", &number);

  switch (number) {
  case 1:
    printf("styczen\n");
    break;
  case 2:
    printf("luty\n");
    break;
  case 3:
    printf("marzec\n");
    break;
  case 4:
    printf("kwiecien\n");
    break;
  case 5:
    printf("maj\n");
    break;
  case 6:
    printf("czerwiec\n");
    break;
  case 7:
    printf("lipiec\n");
    break;
  case 8:
    printf("sierpien\n");
    break;
  case 9:
    printf("wrzesien\n");
    break;
  case 10:
    printf("pazdziernik\n");
    break;
  case 11:
    printf("listopad\n");
    break;
  case 12:
    printf("grudzien\n");
    break;
  default:
      printf("Error\n");
  }
  
  return 0;
}
