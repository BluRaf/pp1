#include <stdio.h>

int main()
{
  int parzyste = 0;
  int nieparzyste = 0;
  int buf;
  
  scanf("%d", &buf);
  while (buf != -1) {
    if (buf%2) {
      nieparzyste += buf;
	}
	else {
      parzyste += buf;
	}
  }
  printf("%d\n", parzyste);
  printf("%d\n", nieparzyste);
  
  return 0;
}
