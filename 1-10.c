#include <stdio.h>

int main()
{
  float a_1, b_1, a_2, b_2;
  
  printf("a_1 = ");
  scanf("%f", &a_1);
  printf("b_1 = ");
  scanf("%f", &b_1);
  printf("a_2 = ");
  scanf("%f", &a_2);
  printf("b_2 = ");
  scanf("%f", &b_2);

  printf("(%.3f + i%.3f)\n\n", a_1, b_1);
  printf("(%.3f + i%.3f)\n\n", a_2, b_2);

  printf("(%.3f + i%.3f)\n", a_1 + a_2, b_1 + b_2);
  printf("(%.3f + i%.3f)\n", a_1 - a_2, b_1 - b_2);
  printf("(%.3f + i%.3f)\n", a_1 * a_2, b_1 * b_2);
  printf("(%.3f + i%.3f)\n", a_1 / a_2, b_1 / b_2);

  return 0;
}
