#include <stdio.h>

int main()
{
  int a[100];
  int a_len = 0;

  int buf;

  printf("podaj liczby: ");
  do {
    scanf("%d", &buf);
    if (buf == -1) {
      break;
    }
    else {
      a[a_len] = buf;
      a_len++;
    }
  } while (a_len < 100);

  int i, j;
  for (i = 0, j = a_len-1; i < j; i++, j--) {
    if (a[i] != a[j]) {
      break;
    }
  }
  
  if (i < j) {
    printf("NIE\n");
  }
  else {
    printf("TAK\n");
  }
  
  return 0;
}
