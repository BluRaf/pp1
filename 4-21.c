#include <stdio.h>

int main()
{
  int min, max;
  int i;
  
  printf("min = ");
  scanf("%d", &min);
  printf("max = ");
  scanf("%d", &max);
  
  for (i = min; i <= max; i++) {
  	printf("%d", i);
  }
  
  return 0;
}

