#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define RAND_LOW -1000
#define RAND_HIGH 1000

int main()
{
  srand((unsigned int)time(NULL));
  for (int i=0;;i++) {
  	int num = (rand() % (RAND_HIGH - RAND_LOW + 1) + RAND_LOW);
  	if (num < 100 || num > 200) {
      printf("%d\n", num);
    }
  }
  
  
  return 0;
}

