#include <stdio.h>

int main() {
	printf("     |");
	for (int x = 1; x <= 10; x++) {
		printf("  %2d |", x);
	}
	printf("\n");
	for (int x = 0; x <= 10; x++) {
		printf("------");
	}
	printf("\n");
	for (int y = 1; y <= 10; y++) {
		printf(" %3d |", y);
		for (int x = 1; x <= 10; x++) {
			printf(" %3d |", x*y);
		}
		printf("\n");
	}
}
