extern void bubble_sort_asc(int tab[], int size);
extern void bubble_sort_desc(int tab[], int size);
extern void bubble_sort(int tab[], int size, int dir);

extern void selection_sort_asc(int tab[], int size);
extern void selection_sort_desc(int tab[], int size);
extern void selection_sort(int tab[], int size, int dir);

extern void insertion_sort_asc(int tab[], int size);
extern void insertion_sort_desc(int tab[], int size);
extern void insertion_sort(int tab[], int size, int dir);
