#include <stdio.h>

int fibonacci(int n)
{
  if (n < 0) {
    return -1;
  }
  if (n == 0) {
    return 0;
  }
  if (n == 1) {
    return 1;
  }
  if (n > 1) {
    return (fibonacci(n-1)+fibonacci(n-2));
  }
}

int main()
{
  int n;
  printf("Ktory wyraz ciągu Fibonacciego chcesz wyznaczyc? ");
  scanf("%d", &n);

  int ret = fibonacci(n);
  if (ret < 0) {
    printf("Error\n");
    return 1;
  }
  else {
    printf("Wynik: %d\n", ret);
  }

  return 0;
}
