#include <stdio.h>

float suma(float a, float b)
{
  return a+b;
}

int main()
{
  float input_a;
  float input_b;
  float output;

  printf("Podaj pierwsza liczbe: ");
  scanf("%f", &input_a);
  printf("Podaj druga liczbe: ");
  scanf("%f", &input_b);

  output = suma(input_a, input_b);

  printf("Wynik: %f\n", output);
  
  return 0;
}

