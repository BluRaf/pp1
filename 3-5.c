#include <stdio.h>

int main()
{
  int number;

  scanf("%d", &number);

  switch (number) {
  case 1:
  case 2:
  case 3:
  case 4:
  case 5:
    printf("Dzień roboczy\n");
    break;
  case 6:
  case 7:
    printf("Weekend\n");
    break;
  default:
    printf("Error\n");
  }
  
  return 0;
}
