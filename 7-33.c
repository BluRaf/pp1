#include <stdio.h>

int clamp(int v, int lo, int hi)
{
  if (v < lo) return lo;
  else if (v > hi) return hi;
  else return v;
}

int main()
{
  int mid, first, second;
  printf("Podaj liczbę: ");
  scanf("%d", &mid);
  printf("Podaj pierwszą granicę: ");
  scanf("%d", &first);
  printf("Podaj drugą granicę: ");
  scanf("%d", &second);
  
  if (first < second) {
    printf("%d\n", clamp(mid, first, second));
  }
  if (second < first) {
    printf("%d\n", clamp(mid, second, first));
  }
  
  return 0;    
}
