#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define RAND_LOW 30
#define RAND_HIGH 40

int main()
{
  srand((unsigned int)time(NULL));
  printf("%d\n", rand() % (RAND_HIGH - RAND_LOW + 1) + RAND_LOW);
  
  return 0;
}

