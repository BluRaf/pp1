#include <stdio.h>

int main()
{
  int a = 5;
  int b = 4;

  printf("%i, %i", a, b);

  printf("%i", a*b);
  printf("%.2f", (float)a/b);
  printf("%i", a+b);
  printf("%i", a-b);

  return 0;
}
