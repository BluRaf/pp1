#include <stdio.h>

int main()
{
  int a, b;
  
  printf("a = ");
  scanf("%d", &a);
  printf("b = ");
  scanf("%d", &b);

  if ( (a + b) * (a - b) == (a*a) - (b*b) )
    printf("tożsamość jest prawdziwa\n");
  else
    printf("tożsamość nie jest prawdziwa\n");

  return 0;
}
