#include <stdio.h>

void znak(int wart)
{
  if (wart == 0) {
    printf("zero\n");
    return;
  }
  if (wart > 0) {
    printf("dodatnia\n");
    return;
  }
  if (wart < 0) {
    printf("ujemna\n");
    return;
  }
}

int main()
{
  int input;
  printf("Podaj liczbe: ");
  scanf("%d", &input);
  znak(input);
  return 0;
}

