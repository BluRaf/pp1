#include <stdio.h>

float suma(float a, float b)
{
  return a+b;
}

float roznica(float a, float b)
{
  return a-b;
}

float iloczyn(float a, float b)
{
  return a*b;
}

float iloraz(float a, float b)
{
  return a/b;
}

int main()
{
  float input_a;
  float input_b;

  printf("Podaj pierwsza liczbe: ");
  scanf("%f", &input_a);
  printf("Podaj druga liczbe: ");
  scanf("%f", &input_b);

  printf("Suma: %f\n", suma(input_a, input_b));
  printf("Roznica: %f\n", roznica(input_a, input_b));
  printf("Iloczyn: %f\n", iloczyn(input_a, input_b));
  printf("Iloraz: %f\n", iloraz(input_a, input_b));
  
  return 0;
}

