#include <stdio.h>

int main()
{
  float a, b;
  
  printf("a = ");
  scanf("%f", &a);
  
  do {
    printf("b = ");
    scanf("%f", &b);
    a -= b;
    printf("a = %f\n", a);
  }
  while (b != 0);
  
  return 0;
}

