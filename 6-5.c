#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main() 
{
  int i,j;
  int a[4][4];
  int b[4][4];
  int c[4][4]={{1,0,0,0},
                      {0,1,0,0},
                      {0,0,1,0},
                      {0,0,0,1}
	              };
  int pom[4][4];
  srand(time(NULL));

  for (i = 0; i < 4; i++) { //macierz A
    for (j = 0; j < 4; j++) {
      a[i][j] = rand() % 10;
      printf("%3d", a[i][j]);
    }
    printf("\n");
  }
  printf("\n");

  for ( i = 0; i < 4; i++) { //macierz B
    for (j = 0; j < 4; j++) {
      b[i][j]=rand() % 10;
      printf("%3d", b[i][j]);
    }
    printf("\n");
  }
  printf("\n");

  for (i = 0; i < 4; i++) { //macierz C
    for(j = 0; j < 4; j++) {
      printf("%3d", c[i][j]);
    }
    printf("\n");
  }
	
//strefa działań na macierzach
  printf("\n");
  for (i = 0; i < 4; i++) { // suma macierzy
    for (j = 0; j < 4; j++) {
      pom[i][j] = a[i][j] + b[i][j];
      printf("%3d", pom[i][j]);
    }
    printf("\n");
  }
  printf("\n");

  for (i = 0; i < 4; i++) { // roznica macierzy
    for (j = 0; j < 4; j++) {
      pom[i][j] = a[i][j] - b[i][j];
      printf("%3d", pom[i][j]);
    }
    printf("\n");
  }
  printf("\n");

  for (i = 0; i < 4; i++) { // suma macierzy ABC
    for (j = 0; j < 4; j++) {
      pom[i][j] = a[i][j] + b[i][j] + c[i][j];
      printf("%3d", pom[i][j]);
    }
    printf("\n");
  }
  printf("\n");

  for (i = 0; i < 4; i++) { // roznica macierzy ABC
    for (j = 0; j < 4; j++) {
      pom[i][j] = a[i][j] - b[i][j] - c[i][j];
      printf("%3d", pom[i][j]);
    }
    printf("\n");
  }
  printf("\n");

  for (i = 0; i < 4; i++) { // macierz -B
    for (j = 0; j < 4; j++) {
      pom[i][j] = -b[i][j];
      printf("%3d", pom[i][j]);
    }
    printf("\n");
  }
  return 0;
}
