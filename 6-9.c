#include <stdio.h>

int main()
{
  int tab[10][10];
  int suma_parz = 0;
  int suma_nieparz = 0;
  
  for (int y = 0; y < 10; y++) {
  	for (int x = 0; x < 10; x++) {
  	  int buf;
  	  scanf("%d", &buf);
  	  tab[y][x] = buf;
    }
  }
  
  for (int y = 0; y < 10; y++) {
  	for (int x = 0; x < 10; x++) {
      if ((x+y)%2) {
      	suma_nieparz += tab[y][x];
	  }
	  else {
	  	suma_parz += tab[y][x];
	  }
    }
  }
  
  printf("%d\n", suma_parz-suma_nieparz);
}

