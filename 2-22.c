#include <stdio.h>
/* to jest
 * komentarz w wielu
 * linijkach
 */


int main()
{
    int age;
    char gender;
    
    printf("Wiek? ");
    scanf("%d", &age);
    getchar();
    printf("Plec? [k,m] ");
    scanf("%c", &gender);
    
    if (age > 1) {
        switch (gender) {
        case 'm':
        case 'M':
  	        if (age > 75) {
  	            printf("2100\n");
  	            break;
  	        }
  	        if (age > 64) {
  	  	        printf("2300\n");
  	  	        break;
            }
            if (age > 20) {
  	  	        printf("2800 - 3200\n");
  	  	        break;
            }
            if (age > 15) {
  	  	        printf("3200 - 3700\n");
  	  	        break;
            }
            if (age > 12) {
  	  	        printf("3000 - 3300\n");
  	  	        break;
            }
            if (age > 9) {
  	  	        printf("2600\n");
  	  	        break;
            }
        case 'k':
        case 'K':
  	        if (age > 75) {
  	            printf("2000\n");
  	            break;
  	        }
  	        if (age > 59) {
  	  	        printf("2200\n");
  	  	        break;
            }
            if (age > 20) {
  	  	        printf("2400 - 2800\n");
  	  	        break;
            }
            if (age > 15) {
  	  	        printf("2500 - 2700\n");
  	  	        break;
            }
            if (age > 12) {
  	  	        printf("2600 - 2800\n");
  	  	        break;
            }
            if (age > 9) {
  	  	        printf("2300\n");
  	  	        break;
            }
        }
        if (age > 6) {
        	printf("2100\n");
		}
		else if (age > 3) {
        	printf("1700\n");
		}
		else {
        	printf("1300\n");
		}
		return 0;
    }
    else {
  	    printf("4\n");
  	    return 1;
    }
}

