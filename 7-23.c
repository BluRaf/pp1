#include <stdio.h>
#include <math.h>

long long silnia(long long n) {
  if (n == 1 || n == 0) return 1;
  else return n*silnia(n-1);
}

float my_sin(float x, int n) {
  float sinus = 0.0;
  for (int k = 0; k <= n; k++) {
    sinus += (pow(-1, k)/silnia((2*k) + 1)) * pow(x, (2*k) + 1);
  }
  return sinus;
}

float my_cos(float x, int n) {
  float cosinus = 0.0;
  for (int k = 0; k <= n; k++) {
    cosinus += pow(-1, k)/silnia(2*k) * pow(x, 2*k);
  }
  return cosinus;
}

int main()
{
  float input_angle;

  printf("Podaj wartosc kata, dla ktorego chcesz obliczyc wartosc funkcji sin i cos: ");
  scanf("%f", &input_angle);

  int i = 0;
  float msin, mcos;
  float close_enough = 0;
  do {
    i++;
    msin = my_sin(input_angle, i);
    mcos = my_cos(input_angle, i);
  } while ((fabs(sin(input_angle)-msin) >= 0.0001) && (fabs(cos(input_angle)-mcos) >= 0.0001));

  printf("%d\n", i);

  return 0;
}

