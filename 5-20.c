#include <stdio.h>

int main()
{
  int buf;
  int array[11];

  for (int c=0;c<=10;c++)
    array[c] = 0;

  for(;;) {
    scanf("%d", &buf);
    if (buf == -1) {
      break;
    }
    if (buf >= 0 && buf <= 10) {
      array[buf]++;
    }
  }

  for (int i = 0; i <= 10; i++)
    printf("%d - %d\n", i, array[i]);

  return 0;
}
