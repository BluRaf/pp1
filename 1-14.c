#include <stdio.h>

int main()
{
  float x, y = 0;

  printf("Podaj liczbe:\n");
  scanf("%f", &x);

  y += 2 * (x * x * x);
  y += -4 * (x * x);
  y += 3 * x;
  y += -7;

  printf("%f\n", y);

  return 0;
}
