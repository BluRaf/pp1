#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX 1001

int find(char tab[], char c)
{
  for (int i = 0; i < strlen(tab); i++)
    if (tab[i] == c) return i;
  return -1;
}

int find_end(char tab[], char c)
{
  for (int i = strlen(tab); i >= 0; i--)
    if (tab[i] == c) return i;
  return -1;
}

int main()
{
  char to_find;
  printf("Podaj szukany znak: \n");
  scanf("%c", &to_find);

  // Tu lepiej byłoby użyć sscanf, bo sam scanf("%s") kończy na najbliższym białym znaku
  char text[MAX];
  printf("Podaj tekst: \n");
  scanf("%s", &text);

  printf("%d\n", find(text, to_find));
  printf("%d\n", find_end(text, to_find));
  printf("%d\n", abs(find_end(text, to_find)-find(text, to_find)));

  return 0;
}
