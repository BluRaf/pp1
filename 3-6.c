#include <stdio.h>

int main()
{
  int a = 0, b = 0;
  char oper;
  int quit = 0;

  printf("--- MENU ---\n"
	 "=: Wprowadzanie danych\n"
	 "+: Dodawanie\n"
	 "-: Odejmowanie\n"
	 "*: Mnożenie\n"
	 "/: Dzielenie\n"
	 "q: Wyjście\n"
	 );
  while (quit != 1)
  {
    printf("> ");
    scanf("%c", &oper);
    switch (oper) {
    case '=':
      /* wprowadzanie danych */
      printf("a = ");
      scanf("%d", &a);
      printf("b = ");
      scanf("%d", &b);
      break;
    case '+':
      /* dodawanie */
      printf("%d %c %d = %d\n", a, oper, b, a + b);
      break;
    case '-':
      /* odejmowanie */
      printf("%d %c %d = %d\n", a, oper, b, a - b);
      break;
    case '*':
      /* mnożenie */
      printf("%d %c %d = %d\n", a, oper, b, a * b);
      break;
    case '/':
      /* dzielenie */
      if (b != 0)
        printf("%d %c %d = %d\n", a, oper, b, a / b);
      else
	printf("Dzielenie przez zero nie jest wspierane w tym wszechświecie.\n");
      break;
    case 'q':
    case 'Q':
      quit = 1;
      break;
    }
    getchar();
  }

  return 0;
}
