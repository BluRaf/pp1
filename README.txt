﻿/  __   __    \
| ( .) ( .) ; |
|    /_     _ |
\     ~    / //
 _________/ /
|_______    |
    |___    |
    |___    |
    |______/


Collection of useless "Hello, world!" mutations.

Included scripts:
- todo.py - lists all exercises that haven't been done
- not_done.sh - lists not finished exercises (with //TODO somewhere in source code)

Included data:
- all.csv - comma-separated list with task numbers (used by todo.py script)
- DROPPED - information about exercises which couldn't be done because of lack of required knowledge

FQA:

Q: error: ‘for’ loop initial declarations are only allowed in C99 or C11 mode
A: `-std=c99` or `-std=c11` (don't even think about using GNU extensions)

Q: Error 1 error C4996: 'scanf': This function or variable may be unsafe. Consider using scanf_s instead. To disable deprecation, use _CRT_SECURE_NO_WARNINGS. See online help for details.
A: `#declare _CRT_SECURE_NO_WARNINGS` in code or `-D_CRT_SECURE_NO_WARNINGS` in preprocessor flags or just use normal compilator without retarded security errors no one cares about (unless somebody will p0wn their application)
