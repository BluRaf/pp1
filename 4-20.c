#include <stdio.h>

int main()
{
  int input;
  int i;
  int counter = 0;
  
  scanf("%d", &input);
  if (input < 2) {
  	printf("Error\n");
  }
  else {
    for (i = 2; i < input; i++) {
      if (input%i != 0) {
      	counter++;
	  }
	}
	if (counter > 0) {
	  printf("NIE\n");
	}
	else {
	  printf("TAK\n");
	}
  }
  return 0;
}

