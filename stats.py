#!/usr/bin/env python3

import csv
import math
from pathlib import Path

all_tasks = 0
with open('all.csv') as csvfile:
    reader = csv.reader(csvfile, delimiter=",")
    tasks = []

    for row in reader:
        tasks.append(row)
        all_tasks += len(row)

    csvfile.close()

# Additional tasks
with open('add.csv') as csvfile:
    reader = csv.reader(csvfile, delimiter=",")

    for row in reader:
        tasks.append(row)
        all_tasks += len(row)

    csvfile.close()


all_done = 0
for sec_index, section in enumerate(tasks):
    sec_done = 0
    for task in section:
        filepath = "{}-{}.c".format(sec_index+1, task)
        file = Path(filepath)
        if file.is_file():
            sec_done += 1
    all_done += sec_done
    print("{}.: {}/{} done, "
          "{}% - {} to 40%".format(
              sec_index+1, sec_done, len(section),
              int((sec_done/len(section))*100),
              ((len(section)*0.4 > sec_done)
               and math.ceil((len(section)*0.4-sec_done))
               or 0)
          ))

percent_done = round(all_done*100/all_tasks, 2)
remaining = math.ceil((all_tasks*0.7)-all_done)

print("--- {}% done, {}/{}"
      " | remaining: {} tasks ---".format(
          percent_done, all_done, all_tasks, remaining))
