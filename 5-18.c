#include <stdio.h>

int main()
{
  int values[1001]; /* max 1000 liczb całkowitych do policzenia + `-1` jako określenie końca danych */
  int elements = 0, min, max, ratio = 1;
  float average = 0;
  
  for (elements=0;;elements++) {
    scanf("%d", &values[elements]);
    if (values[elements] == -1) {
      break;
    }
  }

  printf("%d\n", elements);
  
  for (int i=0;i<elements;i++) {
    if (i==0) {
      min = values[i];
    }
    else {
      if (min > values[i])
	min = values[i];
    }
  }
  printf("%d\n", min);
  
  for (int i=0;i<elements;i++) {
    if (i==0) {
      max = values[i];
    }
    else {
      if (max < values[i])
	max = values[i];
    }
  }
  printf("%d\n", max);
  
  for (int i=0;i<elements;i++) {
    average += values[i];
  }
  average /= (elements-1);
  printf("%f\n", average);
  
  for (int i=0;i<elements;i++) {
    ratio *= values[i];
  }
  printf("%d\n", ratio);

  return 0;
}

  
