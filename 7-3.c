#include <stdio.h>
#include <math.h>

float obwod_kola(float b)
{
  return b*M_PI;
}

int main()
{
  float input;
  printf("Podaj srednice kola: ");
  scanf("%f", &input);
  float output = obwod_kola(input);
  printf("Obwod kola to %f\n", output);
  return 0;
}

