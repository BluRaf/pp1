#include <stdio.h>

int main()
{
  int tab[15][15];
  int result[30];
  
  for (int i = 0; i < 30; i++) {
  	result[i] = 0;
  }
  
  for (int y = 0; y < 15; y++) {
  	for (int x = 0; x < 15; x++) {
  	  int buf;
  	  scanf("%d", &buf);
  	  tab[y][x] = buf;
  	  
  	  if (buf >= 0 && buf < 30) {
  	  	result[buf]++;
	  }
    }
  }
  
  for (int y = 0; y < 15; y++) {
  	for (int x = 0; x < 15; x++) {
  	  if (tab[y][x] >= 0 && tab[y][x] < 30) {
  	  	tab[y][x] = result[tab[y][x]];
	  }
    }
  }
  
  for (int y = 0; y < 15; y++) {
  	for (int x = 0; x < 15; x++) {
  	  printf(" %2d", tab[y][x]);
    }
    printf("\n");
  }
}

