#include <stdio.h>
#include <stdlib.h>

int main() 
{
  int tab[5][5];
  int max[10];
  int i,j;
  
  for (i = 0; i < 10; i++) {
    max[i] = 0;
  }
  
  for (i = 0; i < 5; i++) {
    for (j = 0; j < 5; j++) {
      scanf("%d", &tab[i][j]);
    }
  }
  
  for (i = 0; i < 5; i++) {
    for (j = 0; j < 5; j++) {
      if (tab[i][j] > max[i]) {
        max[i] = tab[i][j];
      }
    }
  }
  
  for (i = 0; i < 5; i++) {
    for (j = 0; j < 5; j++) {
      if (tab[j][i] > max[i+5]) {
        max[i+5]=tab[j][i];
      }
    }
  }
  printf("\n");
  
  for (i = 0; i < 10; i++) {
    printf("%d ", max[i]);
  }
  return 0;
}
