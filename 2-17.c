#include <stdio.h>

float f(float x)
{
  if (x >= -5.0 && x <= 5.0)
    return ((2.0 * (x * x)) + (3.0 * x) - 1.0);
  if (x < -5.0)
    return (((x + 5.0) * (x + 5.0)) - 10.0);
  if (x > 5.0)
    return (((0.5 * x) - 2) * (2 - (0.75 * x)));
}

int main()
{
  float x;
  
  scanf("%f", &x);

  printf("%f\n", f(x));

  return 0;
}
