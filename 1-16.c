#include <stdio.h>

int main()
{
  float celsius, kelwin, fahrenheit;

  printf("Podaj temperatuję w stopniach Celsjusza: ");
  scanf("%f", &celsius);

  kelwin = celsius + 273.15;
  fahrenheit = celsius * 1.8 + 32;

  printf("K = %f\n", kelwin);
  printf("F = %f\n", fahrenheit);

  return 0;
}
