#include <stdio.h>
#include <conio.h>
#include "vector_utils.h"
#include "sort.h"

#define MAX 150

int main()
{
  int vec[MAX];
  printf("Podaj wektor: ");
  int vec_size = read_vector(tab, MAX, 0);

  int directory;
  printf("Podaj kierunek sortowania: ");
  scanf("%i", &directory);

  int method;
  printf("Podaj metode sortowania: ");
  scanf("%i", &method);

  switch(method) {
  case 1:
    bubble_sort(vec, vec_size, directory);
    break;
  case 2:
    selection_sort(vec, vec_size, directory);
    break;
  case 3:
    insertion_sort(vec, vec_size, directory);
    break;
  default:
    printf("Error\n");
    break;
  }	

  return 0;
}

