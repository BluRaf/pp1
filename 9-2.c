#include <stdio.h>
#include "vector_utils.h"

#define MAX 150

void bubble_sort_desc(int tab[], int size)
{
  int buf;
  int n = size;
  int not_yet_sorted = 1;
  do {
    int swapped = 0;
    for (int i = 0; i < n-1; i++) { /* w danym obiegu liczymy od zera do przedostatniej wartej sprawdzenia wartosci */
      if (tab[i] < tab[i+1]) {      /* zamieniamy, jeśli po lewej stronie mamy mniejszą wartość */
        buf = tab[i];
        tab[i] = tab[i+1];
        tab[i+1] = buf;
        swapped = 1;
      }
    }
    if (swapped == 0) not_yet_sorted = 0;
    n--;                            /* robimy tyle obiegów, ile jest elementów w tablicy -1 */
    display_vector(tab, size);
  } while (n > 1 && not_yet_sorted);
}

void bubble_sort_asc(int tab[], int size)
{
  int buf;
  int n = size;
  int not_yet_sorted = 1;
  do {
    int swapped = 0;
    for (int i = 0; i < n-1; i++) { /* w danym obiegu sprawdzamy od pierwszego elementu do ostatniego nieprzesuniętego */
      if (tab[i] > tab[i+1]) {      /* zamieniamy, jeśli po prawej stronie mamy mniejszą wartość */
        buf = tab[i];
        tab[i] = tab[i+1];
        tab[i+1] = buf;
        swapped = 1;
      }
    }
    if (swapped == 0) not_yet_sorted = 0;
    n--;                            /* robimy tyle obiegów, ile jest elementów w tablicy -1 */
    display_vector(tab, size);
  } while (n > 1 && not_yet_sorted); 
}

void bubble_sort(int tab[], int size, int dir)
{
  switch (dir) {
  case 1:
    bubble_sort_asc(tab, size);
    break;
  case 2:
    bubble_sort_desc(tab, size);
    break;
  default:
    printf("Error\n");
    break;
  }
}

int main()
{
  int vec[MAX];
  printf("Podaj wektor: ");
  int vec_size = read_vector(vec, MAX, 0);
 
  int dir;
  printf("Podaj kierunek sortowania: ");
  scanf("%d", &dir);

  bubble_sort(vec, vec_size, dir);

  return 0;
}
