#include <stdio.h>

int main()
{
  char not_a_lisp_code[2137];
  scanf("%s", &not_a_lisp_code);
  
  int deepest = 0;
  int buf = 0;
  int i_n[2137];
  
  // najpierw liczymy najgłębsze zagnieżdżenie
  for (int i = 0; not_a_lisp_code[i] != '\0'; i++) {
  	if (not_a_lisp_code[i] == '(') {
  	  buf++;               // wchodzimy głębiem
  	  if (buf > deepest) { // pobiliśmy rekord, ...
  	  	deepest = buf; // ...podbijamy
	  }
	}
	if (not_a_lisp_code[i] == ')') {
  	  buf--;               // wynurzamy się
	}
  }
  
  printf("%i: ", deepest);
  
  // teraz liczymy, dla jakich indeksów zaczyna się najgłębsze zagnieżdżenie
  buf = 0;
  for (int j = 0; not_a_lisp_code[j] != '\0'; j++) {
  	if (not_a_lisp_code[j] == '(') {
  	  buf++;                  // wchodzimy głębiej
  	  if (buf == deepest) {   // dobiliśmy do docelowego poziomu
  	  	printf("%d ", j); // wypisujemy indeks, gdzie najdalej doszliśmy
	  }
	}
	if (not_a_lisp_code[j] == ')') {
  	  buf--;
	}
  }
  
  return 0;
  
}

