#include <stdio.h>

int main()
{
  float min, max;
  float i;
  
  printf("min = ");
  scanf("%d", &min);
  printf("max = ");
  scanf("%d", &max);
  
  for (i = min; i <= max; i+=0.25) {
  	printf("%d", i);
  }
  
  return 0;
}

