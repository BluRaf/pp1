#include <stdio.h>

int main()
{
  int tab[11][11];
  for (int x = 0; x<=10; x++) {
    for (int y = 0; y<=10; y++) {
    	tab[x][y] = x*y;
    }
  }
  
  for (int x = 0; x<=10; x++) {
    for (int y = 0; y<=10; y++) {
    	printf(" %3d |", tab[x][y]);
    }
    printf("\n");
  }  
}

