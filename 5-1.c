#include <stdio.h>

int main()
{
  int table[11] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
  printf("%d, ", table[0]);
  printf("%d, ", table[1]);
  printf("%d, ", table[2]);
  printf("%d, ", table[3]);
  printf("%d, ", table[4]);
  printf("%d, ", table[5]);
  printf("%d, ", table[6]);
  printf("%d, ", table[7]);
  printf("%d, ", table[8]);
  printf("%d, ", table[9]);
  printf("%d\n", table[10]);
  for (int i = 0; i<=10; i++) {
    printf("%d\n", table[i]);
  }
  return 0;
}
