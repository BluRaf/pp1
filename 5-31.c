#include <stdio.h>

int main()
{
  int a1[101], a2[101];
  int c1 = 0, c2 = 0, c3;
  int buf;

  for (c1 = 0; c1 < 101; c1++) {
    scanf("%d", &buf);
    if (buf == 0)
      break;
    else
      a1[c1] = buf;
  }

  for (c2 = 0; c2 < 101; c2++) {
    scanf("%d", &buf);
    if (buf == 0)
      break;
    else
      a2[c2] = buf;
  }

  if (c1 <= c2)
    c3 = c1;
  if (c1 > c2)
    c3 = c2;
  
  for (int i = 0; i < c3; i++) {
    printf("%d ", a1[i]-a2[i]);
  }

  printf("\n");

  return 0;
}
