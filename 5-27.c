#include <stdio.h>

#define MAX 1000

int main()
{
  int tab[MAX];
  printf("podaj liczby: ");
  int i = 0;
  for (i=0; i<MAX; i++) {
    int buf;
    scanf("%d", &buf);
    if (buf) tab[i] = buf;
    else break;
  }

  int newtab[i];

  newtab[0] = tab[0];                  // kopiujemy pierwszy element
  int o = 0;                           // pozycja na starej tablicy
  int n = 1;                           // pozycja na nowej tablicy
  for (o = 1; o < i; o++) {            // dla każdej pozycji w starej tablicy
    int is_duplicate = 0;                    // flaga "czy wystąpił duplikat"
    for (int j = 0; j<n; j++) {                  // od 0 do obecnej poz. n.tab.
      if (tab[o] == newtab[j]) is_duplicate=1;    // jeśli duplikat, ustaw flagę
    }   
    if (!is_duplicate) {                // jeśli nie było przeniesienia
      newtab[n]=tab[o];                    // przepisz              
      n++;                                 // zwiększ licznik nowej tablicy
    }   
  }
  
  for (int k = 0; k < n; k++) {
    printf("%d ", newtab[k]);
  }
  printf("\n");
  return 0;
}
