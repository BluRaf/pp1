#include <stdio.h>

int main()
{
  int array[1001];
  int buf;
  int counter = 0;

  for (counter = 0; counter < 1001; counter++) {
    scanf("%d", &buf);
    if (buf == 0)
      break;
    else
      array[counter] = buf;
  }

  for (int i = counter; i >= 0; i--) {
    if (i % 2 == 0) {
      printf("%d ", array[i]);
    }
  }

  printf("\n");

  return 0;
}
