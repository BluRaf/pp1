#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX 1001

int search(char tab[], char to_search[])
{
  for (int i = 0; i < strlen(tab); i++) {
    if (tab[i] = to_search[0]) {
      int counter = 0;
      int j = 0;
      for (j = 0; j < strlen(to_search); j++) {
        if (tab[i+j] == to_search[j])
          counter++;
        else
          break;
      }
      if (counter == strlen(to_search))
        return i;
    }
  }
  return -1;
}

int search_end(char tab[], char to_search[])
{
  for (int i = strlen(tab); i >= 0; i--) {
    if (tab[i] = to_search[strlen(to_search)]) {
      int counter = 0;
      int j = 0;
      for (j = 0; j < strlen(to_search); j++) {
        if (tab[i-j] == to_search[strlen(to_search)-j])
          counter++;
        else
          break;
      }
      if (counter == strlen(to_search))
        return i;
    }
  }
  return -1;
}

int main()
{
  // Tu lepiej byłoby użyć sscanf, bo sam scanf("%s") kończy na najbliższym białym znaku
  char text[MAX];
  printf("Podaj tekst: \n");
  scanf("%s", &text);

  char to_find[MAX];
  printf("Podaj szukany znak: \n");
  scanf("%s", &to_find);

  printf("first %d\n", search(text, to_find));
  printf("last %d\n", search_end(text, to_find));

  return 0;
}
