#include <stdio.h>

#define MAX 5

int main()
{
  float buf, max, min;
  float average = 0;
  for (int i=0;i<MAX;i++) {
  	printf("Liczba nr %d: ", i+1);
  	scanf("%f", &buf);
  	
	if (i==0) {
  	  max = buf;
  	  min = buf;
    }
    
    if (buf > max) {
      max = buf;
	}
	
    if (buf < min) {
      min = buf;
	}
    average += buf;
  }
  average /= MAX;
  
  printf("%.2f\n", min);
  printf("%.2f\n", max);
  printf("%.2f\n", average);
  
}

