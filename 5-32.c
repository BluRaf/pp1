#include <stdio.h>

int main()
{
  int a[100];
  int b[100];

  int a_len = 0;
  int b_len = 0;
  
  int buf;
  printf("podaj liczby: ");

  do {
    scanf("%d", &buf);
    if (buf == 0) {
      break;
    }
    else {
      a[a_len] = buf;
      a_len++;
    }
  } while (a_len < 100);

  do {
    scanf("%d", &buf);
    if (buf == 0) {
      break;
    }
    else {
      b[b_len] = buf;
      b_len++;
    }
  } while (b_len < 100);
  
  int c_len;
  if (a_len > b_len) c_len = b_len;
  if (b_len > a_len) c_len = a_len;

  for (int i = 0; i<c_len; i++) {
    printf("%d ", a[i]*b[b_len-1-i]);
  }

  printf("\n");
  return 0;

}
