#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX 1001

int find_any_of(char tab[], char tab2[], int size2)
{
  for (int i = 0; i < strlen(tab); i++) {
    for (int j = 0; j < size2; j++)
      if (tab[i] == tab2[j]) return i;
  }
  return -1;
}

int find_any_of_end(char tab[], char tab2[], int size2)
{
  for (int i = strlen(tab); i >= 0; i--) {
    for (int j = 0; j < size2; j++)
      if (tab[i] == tab2[j]) return i;
  }
  return -1;
}

int main()
{
  // Tu lepiej byłoby użyć sscanf, bo sam scanf("%s") kończy na najbliższym białym znaku
  char text[MAX];
  printf("Podaj tekst: \n");
  scanf("%s", &text);

  char to_find[MAX];
  printf("Podaj szukany ciag znakow: \n");
  scanf("%s", &to_find);

  printf("%d\n", find_any_of(text, to_find, strlen(to_find)));
  printf("%d\n", find_any_of_end(text, to_find, strlen(to_find)));

  return 0;
}
