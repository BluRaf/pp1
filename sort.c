#include <stdio.h>
#include "vector_utils.h"

void bubble_sort_desc(int tab[], int size)
{
  int buf;
  int n = size;
  int not_yet_sorted = 1;
  do {
    int swapped = 0;
    for (int i = 0; i < n-1; i++) { /* w danym obiegu liczymy od zera do przedostatniej wartej sprawdzenia wartosci */
      if (tab[i] < tab[i+1]) {      /* zamieniamy, jeśli po lewej stronie mamy mniejszą wartość */
        buf = tab[i];
        tab[i] = tab[i+1];
        tab[i+1] = buf;
        swapped = 1;
      }
    }
    if (swapped == 0) not_yet_sorted = 0;
    n--;                            /* robimy tyle obiegów, ile jest elementów w tablicy -1 */
    display_vector(tab, size);
  } while (n > 1 && not_yet_sorted);
}

void bubble_sort_asc(int tab[], int size)
{
  int buf;
  int n = size;
  int not_yet_sorted = 1;
  do {
    int swapped = 0;
    for (int i = 0; i < n-1; i++) { /* w danym obiegu sprawdzamy od pierwszego elementu do ostatniego nieprzesuniętego */
      if (tab[i] > tab[i+1]) {      /* zamieniamy, jeśli po prawej stronie mamy mniejszą wartość */
        buf = tab[i];
        tab[i] = tab[i+1];
        tab[i+1] = buf;
        swapped = 1;
      }
    }
    if (swapped == 0) not_yet_sorted = 0;
    n--;                            /* robimy tyle obiegów, ile jest elementów w tablicy -1 */
    display_vector(tab, size);
  } while (n > 1 && not_yet_sorted); 
}

void bubble_sort(int tab[], int size, int dir)
{
  switch (dir) {
  case 1:
    bubble_sort_asc(tab, size);
    break;
  case 2:
    bubble_sort_desc(tab, size);
    break;
  default:
    printf("Error\n");
    break;
  }
}

void selection_sort_desc(int tab[], int size)
{
  int buf;
  int index_max;
  for (int i = 0; i < size; i++) {
    index_max = find_max(tab, size, i);
    if (index_max > i) {
      buf = tab[i];
      tab[i] = tab[index_max];
      tab[index_max] = buf;
    }
  }
  display_vector(tab, size);
}

void selection_sort_asc(int tab[], int size)
{
  int buf; 
  int index_min;
  for (int i = 0; i < size; i++) {
    index_min = find_min(tab, size, i);
    if (index_min > i) {
      buf = tab[i];
      tab[i] = tab[index_min];
      tab[index_min] = buf;
    }
  }
  display_vector(tab, size);	
}

void selection_sort(int tab[], int size, int dir)
{
  switch (dir) {
  case 1:
    selection_sort_asc(tab, size);
    break;
  case 2:
    selection_sort_desc(tab, size);
    break;
  default:
    printf("Error\n");
    break;
  }
}

void insertion_sort_desc(int tab[], int size)
{
  int i, x, c, n;
  for (x = 1; x < size; x++) {
    n = x;
    for (i = x - 1; i >= 0; i--) {
      if (tab[i] < tab[x]) {
        c = tab[i];
        tab[i] = tab[x];
        tab[x] = c;
        x = i;
      }
    }
    x = n;
    display_vector(tab, size);
  }
}

void insertion_sort_asc(int tab[], int size)
{
  int i, x, c, n;
  for (x = 1; x < size; x++) {
    n = x;
    for (i = x - 1; i >= 0; i--) {
      if (tab[i] > tab[x]) {
        c = tab[i];
        tab[i] = tab[x];
        tab[x] = c;
        x = i;
      }
    }
    x = n;
    display_vector(tab, size);
  }
}

void insertion_sort(int tab[], int size, int dir)
{
  switch (dir) {
  case 1:
    insertion_sort_asc(tab, size);
    break;
  case 2:
    insertion_sort_desc(tab, size);
    break;
  default:
    printf("Error\n");
    break;
  }
}
