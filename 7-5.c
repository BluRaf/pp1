#include <stdio.h>

int suma(int a, int b)
{
  return a+b;
}

int main()
{
  int input_a;
  int input_b;
  int output;

  printf("Podaj pierwsza liczbe: ");
  scanf("%d", &input_a);
  printf("Podaj druga liczbe: ");
  scanf("%d", &input_b);

  output = suma(input_a, input_b);

  printf("Wynik: %d\n", output);
  
  return 0;
}

