#include <stdio.h>

int main()
{
  float a, b;
  
  printf("Podaj pierwszą liczbę:\n");
  scanf("%f", &a);
  printf("Podaj drugą liczbę:\n");
  scanf("%f", &b);

  printf("%f\n", a+b);
  printf("%f\n", a-b);
  printf("%f\n", a*b);
  printf("%f\n", a/b);

  return 0;
}
