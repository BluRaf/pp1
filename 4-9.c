#include <stdio.h>

int main()
{
	for (int a = -10; a <= 10; a++) {
		for (int b = -10; b <= 10; b++) {
			if ( ((a+b)*(a-b)) == ((a*a)-(b*b)) ) {
				printf("a = %d; b = %b\n", a, b);
			}
	    }
	}
	return 0;
}

