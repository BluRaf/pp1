#include <stdio.h>

float f(float x)
{
  return ((5*(x*x)) + (12.55*x) + 0.75);
}

int main()
{
  float input;
  printf("x = ");
  scanf("%f", &input);
  printf("f(x) = %f\n", f(input));
  return 0;
}

