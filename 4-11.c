#include <stdio.h>
#define _USE_MATH_DEFINES
#include <math.h>

int main()
{
	float r;
	do {
		printf("r = ");
		scanf("%f", &r);
		if (r < 0)
			printf("Error\n");
	}
	while (r < 0);
	printf("O = %f\n", 2.0*r*M_PI);
	printf("P = %f\n", M_PI*r*r);
}

