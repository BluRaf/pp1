#include <stdio.h>

int main()
{
  int a = 1;
  for (int i=0;i<=9;i++) {
    for (int j=0;j<i;j++) {
      a *= 10;
    }
    printf("10^%d = %d\n", i, a);
    a = 1;
  }
  return 0;
}
