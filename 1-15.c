#include <stdio.h>
#include <math.h>

int main()
{
  float a, b, c;
  float sin_alpha, sin_beta;
  float sin_alpha_deg, sin_beta_deg;
  
  printf("a = ");
  scanf("%f", &a);
  printf("b = ");
  scanf("%f", &b);

  c = sqrt((a*a)+(b*b));
  
  sin_alpha = a/c;
  sin_beta = b/c;
  sin_alpha_deg = asin(sin_alpha)*(180.0/M_PI);
  sin_beta_deg = asin(sin_beta)*(180.0/M_PI);

  printf("c = %f\n", c);
  printf("sin alpha = %f degrees\n", sin_alpha_deg);
  printf("sin beta = %f degrees\n", sin_beta_deg);

  return 0;
}
