#include <stdio.h>

int main()
{
	char letters[5];
	
	for (int i = 0; i < 5; i++) {
		letters[i] = getchar();
	}
	
	for (int i = 0; i < 5; i++) {
		if (letters[i] >= 97 && letters[i] <= 122) {
			letters[i] -= 32;
		}
		printf("%c", letters[i]);
	}
	printf("\n");
	return 0;
}

