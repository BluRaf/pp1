#include <stdio.h>

int main()
{
  float input[10];
  int i;
  for (i=0;i<10;i++) {
    scanf("%f", &input[i]);
  }
  int j;
  for (j=0;j<10;j++) {
    printf("%.2f ", input[j]);
  }
  printf("\n");
}
