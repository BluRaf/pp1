#include <stdio.h>

int main()
{
  int a;

  scanf("%d", &a);

  if (a > 0)
    printf("dodatnia\n");
  if (a < 0)
    printf("ujemna\n");
  if (a == 0)
    printf("zerowa\n");

  return 0;
}
