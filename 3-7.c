#include <stdio.h>

int main()
{
  float pln = 0.00;
  float gbp_value = 4.79;
  float usd_value = 3.60;
  float eur_value = 4.26;
  char oper[4];

  printf("PLN: ");
  scanf("%f", &pln);

  printf("[GBP, USD, EUR]: ");
  scanf("%s", &oper);

  switch (oper) {
  case "GBP":
  case "gbp":
    printf("%.2f GBP\n", pln*gbp_value);
    break;
  case "USD":
  case "usd":
    printf("%.2f USD\n", pln*usd_value);
    break;
  case "EUR":
  case "eur":
    printf("%.2f EUR\n", pln*eur_value);
    break;
  default:
    printf("Unknown currency\n");
  }

  return 0;
}
