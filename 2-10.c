#include <stdio.h>

int main()
{
  float a, b;
  
  printf("Liczba? ");
  scanf("%f", &a);
  printf("Liczba? ");
  scanf("%f", &b);

  printf("%f\n", a+b);
  printf("%f\n", a-b);
  printf("%f\n", a*b);
  if (b == 0.0) {
    printf("Dividing by zero is not supported in this universe.\n");
  }
  else {
    printf("%f\n", a/b);
  }

  return 0;
}
