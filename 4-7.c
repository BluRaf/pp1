#include <stdio.h>

int main()
{
  long long int a = 1;
  int b = 15;
  for (int i=0;i<=b;i++) {
    for (int j=0;j<i;j++) {
      a *= 10;
    }
    printf("10^%*d = %*lli\n", b/10+1, i, b+1, a);
    a = 1;
  }
  return 0;
}
