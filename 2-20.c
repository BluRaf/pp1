#include <stdio.h>

int main()
{
	char letters[5];
	
	for (int i = 0; i < 5; i++) {
		letters[i] = getchar();
	}
	int counter_x = 0, counter_y = 0, counter_z = 0, counter_2 = 0, counter_7 = 0;
	for (int i = 0; i < 5; i++) {
		if (letters[i] >= 65 && letters[i] <= 90) {
			letters[i] += 32;
		}
		switch (letters[i]) {
	    case 'x':
	    	counter_x++;
	    	break;
	    case 'y':
	    	counter_y++;
	    	break;
	    case 'z':
	    	counter_z++;
	    	break;
	    case '2':
	    	counter_2++;
	    	break;
	    case '7':
	    	counter_7++;
	    	break;
		}
	}
	printf("x: %d\n", counter_x);
	printf("y: %d\n", counter_y);
	printf("z: %d\n", counter_z);
	printf("2: %d\n", counter_2);
	printf("7: %d\n", counter_7);
	return 0;
}

