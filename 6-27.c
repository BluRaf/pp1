#include <stdio.h>
#include <stdlib.h>

int main()
{
  int tab[5][10];
  int tran[10][5];
  int i, j;
  
  for (i = 0; i < 5; i++) {
    for (j = 0; j < 10; j++) {
      scanf("%d", &tab[i][j]);
    }
  }
  
  for (i = 0; i < 5; i++) {
    for (j = 0; j < 10; j++) {
      tran[j][i] = tab[i][j];
    }
  }
  printf("\n");
  
  for (i = 0; i < 10; i++) {
    for (j = 0; j < 5; j++) {
      printf("%4d", tran[i][j]);
    }
    printf("\n");
  }
  
  return 0;
}
