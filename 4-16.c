#include <stdio.h>

int main()
{
  unsigned char prev_uchar, uchar = 1;
  unsigned short prev_ushort, ushort = 1;
  unsigned int prev_uint, uint = 1;
  unsigned long int prev_ulint, ulint = 1;
  float prev_f, f = 1.0;
  double prev_d, d = 1.00;
  int i = 1;

  printf("unsigned char: ");
  i = 1;
  for (i=1;;i++) {
    prev_uchar = uchar;
    uchar *= i;
    if (uchar/i != prev_uchar) {
      break;
    }
  }
  printf("%i! = %i\n", i-1, prev_uchar);

  printf("unsigned short: ");
  i = 1;
  for (i=1;;i++) {
    prev_ushort = ushort;
    ushort *= i;
    if (ushort/i != prev_ushort) {
      break;
    }
  }
  printf("%i! = %i\n", i-1, prev_ushort);

  printf("unsigned int: ");
  i = 1;
  for (i=1;;i++) {
    prev_uint = uint;
    uint *= i;
    if (uint/i != prev_uint) {
      break;
    }
  }
  printf("%i! = %i\n", i-1, prev_uint);

  printf("unsigned long int: ");
  i = 1;
  for (i=1;;i++) {
    prev_ulint = ulint;
    ulint *= i;
    if (ulint/i != prev_ulint) {
      break;
    }
  }
  printf("%i! = %li\n", i-1, prev_ulint);

  printf("float: ");
  i = 1;
  for (i=1;;i++) {
    prev_f = f;
    f *= i;
    if (f/i != prev_f) {
      break;
    }
  }
  printf("%i! = %f\n", i-1, prev_f);

  printf("double: ");
  i = 1;
  for (i=1;;i++) {
    prev_d = d;
    d *= i;
    if (d/i != prev_d) {
      break;
    }
  }
  printf("%i! = %lf\n", i-1, prev_d);


}
