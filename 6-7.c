#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main() 
{
  int i,j;
  int a[4][4];
  int b[4][4]; 
  int c[4][4]={{1,0,0,0},
               {0,1,0,0},
               {0,0,1,0},
               {0,0,0,1}
               };
  int suma = 0;
  srand(time(NULL));
  
  for (i = 0; i <= 3; i++) { //macierz A
    for (j = 0; j <= 3; j++) {
      a[i][j] = rand() % 10;
      printf("%d ", a[i][j]);
    }
    printf("\n");
  }
  printf("\n");

  for (i = 0; i <= 3; i++) { //macierz B
    for (j = 0; j <= 3; j++) {
      b[i][j] = rand() % 10;
      printf("%d ", b[i][j]);
    }
    printf("\n");
  }
  printf("\n");

  for (i = 0; i <= 3; i++) { //macierz C
    for (j = 0; j <= 3; j++) {
      printf("%d ", c[i][j]);
    }
    printf("\n");
  }
  
  //działania na macierzach
  printf("\n");
  for (i = 0; i <= 3; i++) { //suma wierszy A
    suma = 0;
    for (j = 0; j <= 3; j++) {
      suma += a[i][j];
    }
    printf("%2d ", suma);
  }
  printf("\n");

  for (i = 0; i <= 3; i++) { //suma kolumn A
    suma = 0;
    for (j = 0; j <= 3; j++) {
      suma += a[j][i];
    }
    printf("%2d ", suma);
  }
  printf("\n");

  for (i = 0; i <= 3; i++) { //suma wierszy B
    suma = 0;
    for (j = 0; j <= 3; j++) {
      suma += b[i][j];
    }
    printf("%2d ", suma);
  }
  printf("\n");

  for (i = 0; i <= 3; i++) { //suma kolumn B
    suma = 0;
    for (j = 0; j <= 3; j++) {
      suma += b[j][i];
    }
    printf("%2d ", suma);
  }
  printf("\n");

  for (i = 0; i <= 3; i++) { //suma wierszy C
    suma = 0;
    for (j = 0; j <= 3; j++) {
      suma += c[i][j];
    }
    printf("%2d ", suma);
  }
  printf("\n");

  for (i = 0; i <= 3; i++) { //suma kolumn C
    suma = 0;
    for (j = 0; j <= 3; j++) {
      suma += c[j][i];
    }
    printf("%2d ", suma);
  }
  printf("\n");

  return 0;
}
