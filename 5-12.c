#include <stdio.h>

int main()
{
  int values[10];
  int max, min;
  float average = 0;
  /* pobieranie wartości */
  for (int i = 0; i < 10; i++) {
    scanf("%d", &values[i]);
  }

  /* wyznaczanie średniej */
  for (int j = 0; j < 10; j++) {
    average += values[j];
  }
  average /= 10.0;

  /* wyznaczanie minimalnej */
  for (int k = 0; k < 10; k++) {
    if (k == 1) {
      min = values[k];
    }
    else {
      if (min > values[k]) {
	min = values[k];
      }
    }
  }
  
  /* wyznaczanie maksymalnej */
  for (int l = 0; l < 10; l++) {
    if (l == 1) {
      max = values[l];
    }
    else {
      if (max < values[l]) {
	max = values[l];
      }
    }
  }

  printf("%f\n%d\n%d\n", average, min, max);
  return 0;
}
