#include <stdio.h>

int czy_pierwsza(int a)
{
  int i;
  if (a > 1) {
    for (i = 2; i*i <= a; i++) {
      if (a%i == 0) {
        return 0;
      }
    }
    return 1;
  }
  else {
    return 0;
  }
}

int main()
{
  int x1, x2;
  int min, max;

  printf("x1 = ");
  scanf("%d", &x1);
  printf("x2 = ");
  scanf("%d", &x2);

  if (x1 < x2) {
    min = x1;
    max = x2;
  }
  else {
    min = x2;
    max = x1;
  }

  for (int i = min; i <= max; i++) {
    if (czy_pierwsza(i)) printf("%d ", i);
  }

  printf("\n");
  return 0;
}
