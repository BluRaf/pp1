#include <stdio.h>

int concat_begin(int first[], int size, int second[], int size2, int dest[], int size3)
{
  if (size+size2 > size3) {
    return -1;
  }
  else {
    int counter = 0;
    for (int i = 0; i < size2; i++) {
      dest[i] = second[i];
      counter++;
    }
    for (int j = 0; j < size; j++) {
      dest[size2+j] = first[j];
      counter++;
    }
    return counter;
  }
}

int concat_end(int first[], int size, int second[], int size2, int dest[], int size3)
{
  if (size+size2 > size3) {
    return -1;
  }
  else {
    int counter = 0;
    for (int i = 0; i < size; i++) {
      dest[i] = first[i];
      counter++;
    }
    for (int j = 0; j < size2; j++) {
      dest[size+j] = second[j];
      counter++;
    }
    return counter;
  }
}

int concat_zip(int first[], int size, int second[], int size2, int dest[], int size3)
{
  if (size+size2 > size3) {
    return -1;
  }
  else {
    int i = 0;
    int j = 0;
    int counter = 0;
    for (counter = 0; counter < size+size2;) {
      if (i < size) {
        dest[counter] = first[i];
        i++;
        counter++;
      }
      if (j < size2) {
        dest[counter] = second[j];
        j++;
        counter++;
      }
    }
    return counter;
  }
}

int read_vector(int vec[], int size, int stop_value)
{
  int i = 0;
  int buf;
  for (i = 0; i < size; i++) {
    scanf("%d", &buf);
    vec[i] = buf;
    if (buf == stop_value) break;
  }
  return i;
}

void display_vector(int vec[], int size)
{
  for (int i = 0; i<size; i++) {
    printf("%d ", vec[i]);
  }
  printf("\n");
}

int find_min(int tab[], int size, int start_index)
{
  int min = tab[start_index];
  int index = start_index;
  for(int i = start_index; i < size; i++) {
    if(tab[i] < min) {
      min = tab[i];
      index = i;
    }
  }
  return index;
}

int find_max(int tab[], int size, int start_index)
{
  int max = tab[start_index];
  int index = start_index;
  for(int i = start_index; i < size; i++) {
    if(tab[i] > max) {
      max = tab[i];
      index = i;
    }
  }
  return indeks;
}
