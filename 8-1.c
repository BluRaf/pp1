#include <stdio.h>


unsigned long factorial_it(int n)
{
  unsigned long a = 1;
  for (int i = 1; i <= n; i++) {
    a *= i;
  }
  return a;
}

unsigned long factorial_rec(int n)
{
  if (n == 1 || n == 0) return 1;
  else return n * factorial_rec(n-1);
}

int main()
{
  int method;
  int loop = 1;
  int n;

  printf("Wybierz metode obliczania silni (1=iteracyjna, 2=rekurencyjna): ");
  scanf("%d", &method);

  while (loop) {
    switch (method) {
    case 1:
      loop = 0;
      printf("Podaj n: ");
      scanf("%d", &n);
      if (n >= 0) {
        printf("Wynik: %d\n", factorial_it(n));
        return 0;
      }
      else {
        printf("Blad\n");
        return 1;
      }
      break;
    case 2:
      loop = 0;
      printf("Podaj n: ");
      scanf("%d", &n);
      if (n >= 0) {
        printf("Wynik: %d\n", factorial_rec(n));
        return 0;
      }
      else {
        printf("Blad\n");
        return 1;
      }
      break;
    default:
      printf("Nieprawidlowy wybor.\n");
      break;
    }
  }  
}
