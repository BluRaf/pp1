#include <stdio.h>

float pobierz_wzrost()
{
  int done = 0;
  float input;
  do {
    printf("Podaj wzrost w metrach: ");
    scanf("%f", &input);
    if (input < 1.10 || input > 2.75) {
      printf("Wartosc poza zakresem. Sprobuj ponownie.\n");
    }
    else {
      done = 1;
    }
  } while (!done);
  return input;
}


int main()
{
  float wzrost_m = pobierz_wzrost();
  float wzrost_cm = wzrost_m*100;
  float wzrost_inch = wzrost_cm/2.54;
  float wzrost_feet = wzrost_inch/12;
  float reszta_wzrostu_w_calach = wzrost_inch - ((int)wzrost_feet*12);
  
  
  printf("Wzrost w jednostkach imperialnych to %d'%d\"\n", (int)wzrost_feet, reszta_wzrostu_w_calach);
  return 0;
}

