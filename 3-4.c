#include <stdio.h>

int main()
{
  int number;

  scanf("%d", &number);

  switch (number) {
  case 1:
    printf("Brak promocji do nastepnej klasy\n");
    break;
  case 2:
  case 3:
  case 4:
  case 5:
    printf("Promocja do nastepnej klasy\n");
    break;
  case 6:
    printf("Promocja z ocena celujaca\n");
    break;
  default:
    printf("Wprowadzona ocena jest niepoprawna\n");
  }
  
  return 0;
}
